
public class BubbleANDInsert {

	public static void main(String[] args) {
		int[] a1 = { 2, 3, 1, 4, 6, 5, 7, 6, 9, 8 };
		int[] a2 = { 4, 2, 3, 1, 8, 6, 7, 5, 9 };

		int[] b1 = bubbleSort(a1);
		int[] b2 = insertionSort(a2);
		
		for(int x=0; x<b1.length; x++)
		{
			System.out.println(b1[x]);
		}
		
		for(int x=0; x<b2.length; x++)
		{
			System.out.println(b2[x]);
		}

	}

	public static int[] bubbleSort(int[] arr) {
		int as1 = 0;
		int as2 = 0;
		for (int x = 0; x < arr.length; x++) {
			as2++;
			for (int y = 0; y < arr.length - as2; y++) {
				if (arr[y] > arr[y + 1]) {
					as1 = arr[y];
					arr[y] = arr[y + 1];
					arr[y + 1] = as1;

				}
			}
		}
		return arr;
	}

	public static int[] insertionSort(int[] arr) {
		int as1 = 0;
		int as2 = 0;
		for (int x = 1; x < arr.length; x++) {
			as2 = x;
			while ((as2 > 0) && (arr[as2 - 1] > arr[as2])) {
				as1 = arr[as2 - 1];
				arr[as2 - 1] = arr[as2];
				arr[as2] = as1;
				as2--;
			}
		}
		return arr;
	}
}
