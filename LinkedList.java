
public class LinkedList {
	Node head;
	Node tail;
	
	public LinkedList()
	{
		head=null;
		tail=null;
	}
	
	public void add(int i)
	{
		Node n = new Node(i);
		if(head==null)
		{
			head=n;
		}
		else
		{
			Node a = head;
			while(a.next!=null){
				a=a.getNext();
			}
			a.setNext(new Node(i));
		}
	}
	
	public void print()
	{
		Node a = head;
		System.out.println(a.getValue());
		while(a.next!=null){
			a=a.getNext();
			System.out.println(a.getValue());
		}
	}
}
