import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class DBManager {
	private Connection c;
	Scanner sc1 = new Scanner(System.in);
	Scanner sc2 = new Scanner(System.in);
	Scanner sc3 = new Scanner(System.in);
	Scanner sc4 = new Scanner(System.in);

	public DBManager() {
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:C:\\sqlite\\sqlite-tools-win32-x86-3150100\\timetracker.db");
		} catch (SQLException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public void createTime() {
		PreparedStatement stmt;
		String sql = "CREATE TABLE IF NOT EXISTS Time("+
					"TimeID INT PRIMARY KEY, " +
					"StartTime TEXT NOT NULL, " +
					"EndTime TEXT NOT NULL, " +
					"FSTopicID INT REFERENCES Topic(TopicID));";
		try {
			stmt = c.prepareStatement(sql);
			stmt.executeUpdate();
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void createTopic() {
		PreparedStatement stmt;
		String sql = "CREATE TABLE IF NOT EXISTS Topic("+
					"TopicID INT PRIMARY KEY, " +
					"Name TEXT NOT NULL, " +
					"RFID TEXT NOT NULL)";
		try {
			stmt = c.prepareStatement(sql);
			stmt.executeUpdate();
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void newTopic() {
		System.out.println("TopicID eingeben.");
		int k = sc1.nextInt();
		System.out.println("Name eingeben");
		String n = sc2.nextLine();
		System.out.println("RFID eingeben");
		String r = sc3.nextLine();
		PreparedStatement stmt = null;
		try {
			String sql = "INSERT INTO Topic(TopicID, Name, RFID) VALUES(?,?,?)";
			stmt = c.prepareStatement(sql);
			stmt.setInt(1, k);
			stmt.setString(2, n);
			stmt.setString(3, r);
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println("Erfolgreich erstellt.");
		System.out.println("");
	}
	
	public void newTime() {
		System.out.println("TimeID eingeben.");
		int i1 = sc1.nextInt();
		System.out.println("Startzeit eingeben. (zB.: 2007-01-01 10:00:00)");
		String s1 = sc2.nextLine();
		System.out.println("Endzeit eingeben. (zB.: 2007-01-01 10:30:00)");
		String s2 = sc3.nextLine();
		System.out.println("ID des Topics eingeben.");
		int i2 = sc4.nextInt();
		PreparedStatement stmt = null;
		try{
			String sql = "INSERT INTO Time(TimeID, StartTime, EndTime, FSTopicID) VALUES(?,?,?,?)";
			stmt = c.prepareStatement(sql);
			stmt.setInt(1,i1);
			stmt.setString(2, s1);
			stmt.setString(3, s2);
			stmt.setInt(4, i2);
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println("Erfolgreich erstellt.");
		System.out.println("");
		}
	
	public void deleteTopic() {
		System.out.println("ID des zu löschenden Topics eingeben.");
		int i1 = sc1.nextInt();
		PreparedStatement stmt = null;
		String sDEL= "DELETE FROM Topic WHERE TopicID = " + i1;
		try {
			stmt = c.prepareStatement(sDEL);
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void deleteTime() {
		System.out.println("ID der zu löschenden Time eingeben.");
		int i1 = sc1.nextInt();
		PreparedStatement stmt = null;
		String sDEL= "DELETE FROM Time WHERE TimeID = " + i1;
		try {
			stmt = c.prepareStatement(sDEL);
			stmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void showTime() {
		System.out.println("TimeID-StartTime-EndTime-TopicID");
		String show = "SELECT * FROM TIME";
		try {
			Statement stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery(show);
			while (rs.next()) {
				System.out.println(rs.getInt(1) + "-" + rs.getString(2) + "-" + rs.getString(3) + "-" + rs.getInt(4));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println("");
	}
	
	public void showTopic() {
		System.out.println("TopicID-Name-RFID");
		String show = "SELECT * FROM TOPIC";
		try {
			Statement stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery(show);
			while (rs.next()) {
				System.out.println(rs.getInt(1) + "-" + rs.getString(2) + "-" + rs.getString(3));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println("");
	}
	
	public void updateTime() {
		System.out.println("TimeID eingeben.");
		int a = sc1.nextInt();
		System.out.println("neue StartTime eingeben.");
		String b1 = sc2.nextLine();
		System.out.println("neue EndTime eingeben");
		String b2 = sc3.nextLine();
		System.out.println("neue TopicID eingeben.");
		int b3 = sc4.nextInt();
		try{
			Statement stmt = c.createStatement();
			String sql = "UPDATE TIME " +
						"SET StartTime="+b1 +
						", EndTime=" + b2 +
						", TopicID=" + b3 + 
						"WHERE TimeID=" + a;
			stmt.executeUpdate(sql);
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
		System.out.println("Updated successfully.");
	}

	public void updateTopic() {
		System.out.println("TopicID eingeben.");
		int a = sc1.nextInt();
		System.out.println("neuen Namen eingeben.");
		String b1 = sc2.nextLine();
		System.out.println("neue RFID eingeben.");
		String b2 = sc3.nextLine();
		try{
			Statement stmt = c.createStatement();
			String sql = "UPDATE TOPIC " +
						"SET Name="+b1 +
						", RFID=" + b2 + 
						"WHERE TimeID=" + a;
			stmt.executeUpdate(sql);
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
		System.out.println("Updated successfully.");
	}
}