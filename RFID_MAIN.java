import java.util.Scanner;

public class RFID_MAIN {
	static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		DBManager db = new DBManager();
		db.createTopic();
		db.createTime();
		while(true){
			System.out.println("Bitte w�hlen Sie eine Operation aus:");
			System.out.println();
			System.out.println("Topic erstellen: 1");
			System.out.println("Time erstellen: 2");
			System.out.println("Topic l�schen: 3");
			System.out.println("Time l�schen: 4");
			System.out.println("Topic updaten: 5");
			System.out.println("Time updaten: 6");
			System.out.println("Alle Topics anzeigen: 7");
			System.out.println("Alle Times anzeigen: 8");
			System.out.println();
			String st = sc.nextLine();
			switch (st) {
			case "1":
				db.newTopic();
				break;
			case "2":
				db.newTime();
				break;
			case "3":
				db.deleteTopic();
				break;
			case "4":
				db.deleteTime();
				break;
			case "5":
				db.updateTopic();
				break;
			case "6":
				db.updateTime();
				break;
			case "7":
				db.showTopic();
				break;
			case "8":
				db.showTime();
				break;
			}
		}

	}

}
